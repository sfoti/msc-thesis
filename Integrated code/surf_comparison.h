// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#ifndef SRC_POINTS_MATCHING_SURF_COMPARISON_H_
#define SRC_POINTS_MATCHING_SURF_COMPARISON_H_

#include <vector>

#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/xfeatures2d.hpp>

void comparisonWithSURF(cv::Mat img_left, cv::Mat img_right,
	cv::Mat img_seg_left);
cv::Mat find3dPoints(std::vector<cv::Point2d> coord_left,
	std::vector<cv::Point2d> coord_right);

#endif  // SRC_POINTS_MATCHING_SURF_COMPARISON_H_
