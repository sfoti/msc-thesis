// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>
/*
#ifndef SRC_TIP_POSITION_CONTROL_MICRON_H_
#define SRC_TIP_POSITION_CONTROL_MICRON_H_

#include <list>
#include <opencv2/opencv.hpp>

void controlMicron(cv::Mat all_centers, std::list<cv::Point3d> all_points);
cv::Point3d computeGoalPosition(double dist, cv::Point3d versor,
	cv::Point3d tip);
void waitConnection();

#endif  // SRC_TIP_POSITION_CONTROL_MICRON_H_
*/