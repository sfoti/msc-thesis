// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include <windows.h>
#include <thread>
#include <cmath>
#include <stdio.h>
#include <cassert>
#include <ctime>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "vision_and_control.h"

#include "estimate_distance_tip_structure.h"
#include "micronrecv.h"
#include "micronsend.h"
#include "vision.h"
#include "3d_visualization.h"

const double SAFETY_DISTANCE = 2000;  // in um

// Communication with Micron (receiving and sending)
MicronRecvPacket packetIn;
MicronRecvSocket micronIn;
MicronSendPacket packetOut;
MicronSendSocket micronOut;

cv::Mat all_centers_old;
std::list<cv::Point3d> all_points_old;
bool written_cloud = false;
bool first_cloud_created = false;

void controlMicronWithVision() {
	cv::Mat all_centers_control;
	std::list<cv::Point3d> all_points_control;
	std::thread vision_thread(cloudComputation, std::ref(all_points_old), 
		                      std::ref(all_centers_old), std::ref(written_cloud));
	// Start listening on default port and connect to Micron realtime target
	micronIn.start();
	micronOut.start("192.168.1.101");
	std::ofstream out_traj("trajectory.txt");
	std::ofstream out_no_cont("trajectory_no_control.txt");
	std::ofstream out_closest("closest_point_of_cloud.txt");
	while (true) {
		if (written_cloud) {
			all_centers_control = all_centers_old;
			all_points_control = all_points_old;
			written_cloud = false;
			first_cloud_created = true;
			/*
			// Save cloud in txt
			std::ofstream out("cloud.txt");
			for (const auto& point : all_points_control) {
				out << point;
			}
			out << "end";
			out.close();
			std::cout << "cloud saved";
			*/
		}
		if (micronIn.recv(packetIn) && first_cloud_created) {
			while (micronIn.recv(packetIn));
			cv::Point3d tip_position;			
			tip_position.x = packetIn.goal_force_tip[0];
			tip_position.y = packetIn.goal_force_tip[1];
			tip_position.z = packetIn.goal_force_tip[2];
			
			cv::Point3d tip_position_for_visualization;
			tip_position_for_visualization.x = packetIn.position_tip[0];
			tip_position_for_visualization.y = packetIn.position_tip[1];
			tip_position_for_visualization.z = packetIn.position_tip[2];
			
			showCloudAndTip(all_points_control, tip_position_for_visualization);

			double distance_tip_cloud;
			cv::Point3d versor_point_to_tip;
			// estimateTipDistanceAndNormal(tip_position, all_centers_control, 
			// 	all_points_control,	&distance_tip_cloud, &versor_point_to_tip);
			cv::Point3d closest_point, closest_center;
			estimateTipDistanceNormalAndClosest(tip_position, all_centers_control,
				all_points_control,	&distance_tip_cloud, &versor_point_to_tip, &closest_point, &closest_center);

			// std::cout << "received pos: " << tip_position << " \t | ";
			// std::cout << "distance: " << distance_tip_cloud << " um" << " \t | ";

			// Save trajectories in txt (open file outside the while)		
			out_traj << tip_position_for_visualization;
			out_no_cont << tip_position;
			out_closest << tip_position - distance_tip_cloud*versor_point_to_tip;

			if (distance_tip_cloud <= SAFETY_DISTANCE) { // && versor_point_to_tip.dot(closest_center - closest_point) > 0
				cv::Point3d tip_goal = computeGoalPosition(distance_tip_cloud, versor_point_to_tip, tip_position);
				// std::cout << "ATTENTION!!!" << tip_goal << std::endl;
				// std::cout << "new distance: " << distance_tip_cloud + cv::norm(tip_goal - tip_position) << " um" << std::endl;
				packetOut.goalPos[0] = tip_goal.x;
				packetOut.goalPos[1] = tip_goal.y;
				packetOut.goalPos[2] = tip_goal.z;
				// If this flag is not on, Micron will default to normal behavior and will disregard our goal positions
				packetOut.useGoalPos = 1;
				packetOut.extraInfo[0] = 0; // Turn laser off 
				micronOut.send(packetOut);
			}
			else {				
				cv::Point3d goal(packetIn.goal_force_tip[0], packetIn.goal_force_tip[1], packetIn.goal_force_tip[2]);
				for (int i = 0; i < 3; i++) {
					packetOut.goalPos[i] = packetIn.goal_force_tip[i];
				}				
				packetOut.useGoalPos = 0;
				// std::cout << "normal operation" << goal << std::endl;
				micronOut.send(packetOut);
			}
		}
		else {
			waitConnection();
		}
	}
	vision_thread.join();
}

cv::Point3d computeGoalPosition(double dist, cv::Point3d versor,
	cv::Point3d tip) {
	return (((SAFETY_DISTANCE - dist) * versor) + tip);  // in um
}

void waitConnection() {
	std::cout << "Waiting for connection" << std::flush;
	for (int i = 0; i < 3; i++) {
		std::cout << ".";
		Sleep(200);
	}
	std::cout << "\b\b\b" << "   ";
	std::cout << "\r";
	Sleep(200);
}

