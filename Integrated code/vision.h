// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#ifndef SRC_TIP_POSITION_AND_CONTROL_VISION_H_
#define SRC_TIP_POSITION_AND_CONTROL_VISION_H_

#include <list>
#include <vector>
#include <opencv2/opencv.hpp>

void cloudComputation(std::list<cv::Point3d>& all_points_old, cv::Mat& all_centers_old, bool& written_cloud);
void initialization(cv::Mat* warp_matrix);
void reconstruct(cv::Mat& warp_matrix, std::list<cv::Point3d>* cloud_of_points,
	cv::Mat* all_centers);
void structure3dReconstruction(cv::Mat& img_seg_l, cv::Mat warp_m,
	std::list<cv::Point3d>* all_points,
	cv::Mat* all_centers);
void openingAndClosingInterface();
void changeOpeningAndClosing(int, void*);
cv::Mat morphologicalOperations(cv::Mat& pre);
void printTipTriangulation();
void testReconstruction();

#endif  // SRC_TIP_POSITION_AND_CONTROL_VISION_H_
