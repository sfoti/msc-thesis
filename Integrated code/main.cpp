// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>
/*
#include <stdio.h>
#include <cassert>
#include <ctime>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <filesystem>
#include <string>

#include "branch3d.h"
#include "compute_circumferences_points.h"
#include "control_micron.h"
#include "points_matching_surf.h"
#include "thresholding_segmentation.h"
#include "thinning_zs.h"
#include "estimate_distance_tip_structure.h"
#include "tracking.h"
#include "branch.h"
#include "explore_skeleton.h"
#include "vessel_graph_builder.h"
#include "3d_visualization.h"
#include "unet_segmentation.h"
#include "camera.h"

void initialization(cv::Mat* warp_matrix);
void reconstruct(cv::Mat& warp_matrix);
void structure3dReconstruction(cv::Mat img_seg_l, cv::Mat warp_m,
	                           std::list<cv::Point3d>* all_points,
	                           cv::Mat* all_centers);
void printTipTriangulation();
*/
#include <iostream>
#include "vision_and_control.h"

int main() {
	// TESTS
	/*
	cv::Mat imgL =
		imread("left.png", cv::IMREAD_COLOR);
	cv::Mat imgSegL =
		imread("leftSeg.png", cv::IMREAD_GRAYSCALE);
	cv::threshold(imgSegL, imgSegL, 125, 255, CV_THRESH_BINARY);
	cv::Mat imgR =
		imread("right.png", cv::IMREAD_GRAYSCALE);
     
	// TEST CAMERAS
	openCameras();
	cv::Mat image_left, image_right;
	takeBothPictures(&image_left, &image_right);
	image_left = takeLeftPicture();
	
	// TEST NEURAL NETWORK
	initializeNeuralNetwork();
    cv::Mat image_left_seg = segment(imgL);
	cv::imshow("segmented", image_left_seg);
	cv::waitKey();
	
	// TEST RECONSTRUCTION

	//printTipTriangulation();
	cv::Mat warp_matrix = warpMatrix(imgL, imgR);
	// warpImageL2R(imgL, imgR, imgSegL);
	int t1 = clock();

	std::list<cv::Point3d> cloud_of_points;
	cv::Mat all_centers;
	structure3dReconstruction(image_left_seg, warp_matrix, &cloud_of_points,
		&all_centers);
	
	int t2 = clock();
	std::cout << "time SIMO 3D reconstruction: "
		<< (t2 - t1) / static_cast<double>(CLOCKS_PER_SEC) << std::endl;

	// showCloud(cloud_of_points);

	std::cout << "number of centers: " << all_centers.rows << std::endl;
	std::cout << "number of points: " << cloud_of_points.size() << std::endl;
	std::cout << "end of reconstruction" << std::endl;

	controlMicron(all_centers, cloud_of_points);
	*/
	// TEST ALL TOGETHER
	/*
	cv::Mat warp_matrix;
	initialization(&warp_matrix);
	reconstruct(warp_matrix);
	*/
	// TEST TIME FOR N SEGMENTATIONS
	/*
	std::string path = "C:/Users/sfoti/Desktop/U-NET/test_aug";
	initializeNeuralNetwork();
	for (auto& p : std::experimental::filesystem::v1::directory_iterator(path)) {
		cv::Mat img = imread(p.path().string(), cv::IMREAD_COLOR);
		cv::Mat image_left_seg = segment(img);
	}
	*/
	// TEST THRESHOLDING SEGMENTATION
	/*
	cv::Mat v, n;
	cv::Mat imgSegThTest =
	imread("left1.png", cv::IMREAD_COLOR);
	segmetStructures(imgSegThTest, &v, &n);

	cv::imshow("vessels", v);
	cv::imshow("nerves", n);
	cv::waitKey(0);
	*/
	// COMPARISION WITH SURF
	/*
	int t5 = clock();
	comparisonWithSURF(imgL, imgR, imgSegL);
	int t6 = clock();
	std::cout << "time for SURF reconstruction: "
		<< (t6 - t5) / static_cast<double>(CLOCKS_PER_SEC) << std::endl;
	*/
	// TEST TRACKING
	/*
	cv::Mat imgL2 = imread("sx1rot.png", cv::IMREAD_GRAYSCALE);
	trackTwoImages(imgL, imgL2, imgSegL);

	// VIDEO PART

	cv::VideoCapture cap("leftVideo.avi");
	if (!cap.isOpened()) {
	std::cout << "Error opening video stream or file" << std::endl;
	return -1;
	}
	cv::Mat previous_frame, previous_segmentation;
	cv::Mat current_frame;
	cv::Mat current_segmentation =
	imread("leftSegFrame1.png", cv::IMREAD_GRAYSCALE);
	bool flag_first_frame = true;
	while (1) {
	previous_frame = current_frame;
	cap >> current_frame;
	if (current_frame.empty()) break;
	cv::cvtColor(current_frame, current_frame, cv::COLOR_BGR2GRAY);
	if (flag_first_frame == true) {
	flag_first_frame = false;
	}
	else {
	previous_segmentation = current_segmentation;
	current_segmentation =
	segmentNewFrame(previous_frame, current_frame, previous_segmentation);
	cv::imshow("Frame", current_segmentation);
	}
	char c = static_cast<char>(cv::waitKey(25));
	if (c == 27) break;
	}
	cap.release();
	cv::destroyAllWindows();

	// END VIDEO PART
	*/

	controlMicronWithVision();
	std::getchar();
	return 0;
}
/*
void initialization(cv::Mat* warp_matrix) {
	initializeNeuralNetwork();
	openCameras();
	cv::Mat image_left, image_right;
	takeBothPictures(&image_left, &image_right);
	*warp_matrix = warpMatrix(image_left, image_right);
}

void reconstruct(cv::Mat& warp_matrix) {
	cv::Mat image_left = takeLeftPicture();
	cv::Mat image_left_seg = segment(image_left);
	std::list<cv::Point3d> cloud_of_points;
	cv::Mat all_centers;
	structure3dReconstruction(image_left_seg, warp_matrix, &cloud_of_points,
		&all_centers);
	std::cout << "number of points: " << cloud_of_points.size() << std::endl;
	//controlMicron(all_centers, cloud_of_points);
}

void structure3dReconstruction(cv::Mat img_seg_l, cv::Mat warp_m,
	std::list<cv::Point3d>* all_points,
	cv::Mat* all_centers) {
	cv::Mat vessels_centers_L, vessels_radii_L;
	thinning(img_seg_l, vessels_centers_L);

	cv::imwrite("scheletro.png", vessels_centers_L);

	cv::distanceTransform(img_seg_l, vessels_radii_L, CV_DIST_L2, 3);
	std::list<Branch> branchesOfAllTrees =
		exploreSkeleton(vessels_centers_L, vessels_radii_L);
	cv::Mat fake_center = (cv::Mat_<double>(1, 3) << 0, 0, 0);
	cv::Mat all_centers_plus_one = (cv::Mat_<double>(1, 3) << 0, 0, 0);
	for (const auto& element : branchesOfAllTrees) {
		Branch3d currentBranch(element, warp_m);
		if (currentBranch.getCentersCoord3d().rows > 5) {
			all_points->splice(all_points->end(),
				findAllCrfsOfBranchMean3(currentBranch));
			cv::Mat ctrs = currentBranch.getCentersCoord3d();
			cv::vconcat(all_centers_plus_one, ctrs.rowRange(1, ctrs.rows - 1),
				all_centers_plus_one);
			if (ctrs.rows % 2 != 0) {
				cv::vconcat(all_centers_plus_one, fake_center, all_centers_plus_one);
			}
		}
	}
	*all_centers = all_centers_plus_one.rowRange(1, all_centers_plus_one.rows);
}

void printTipTriangulation() {
	cv::Mat LeftPos =
		(cv::Mat_<double>(4, 3) <<
			-9.636171408551672e-004, 2.081337716803702e-002, -8.539087205345961e-008,
			1.878749903850640e-002, 9.170633241855522e-004, 1.989476307833463e-006,
			9.245645757270630e-003, 2.054529632898400e-004, -2.143388152827867e-006,
			5.664532250675504e+002, 1.528194953118994e+002, 3.433866827349168e-001

			);
	cv::Mat RightPos =
		(cv::Mat_<double>(4, 3) <<
			-1.366552963756280e-003, 2.110787338231304e-002, -4.862283713576373e-007,
			2.019979993495590e-002, 1.079685951179589e-003, 1.819952960344515e-006,
			7.493962720095614e-003, 6.707941685443547e-004, -1.370826161003025e-006,
			4.416107687906746e+002, 1.975835260575813e+002, 4.150422295822854e-001);
	
	cv::Point2d Lc(365, 314);
	cv::Point2d Rc(435,326);
	cv::Mat absolute_4d_coord;

	// triangulation
	cv::triangulatePoints(
		LeftPos.t(), RightPos.t(), cv::Mat(Lc),
		cv::Mat(Rc), absolute_4d_coord);
	cv::Mat homog_coord = absolute_4d_coord.t();
	cv::Mat euclid_coord;
	cv::convertPointsFromHomogeneous(homog_coord.reshape(4), euclid_coord);
	std::cout << "main -> triangulation of the tip: " << euclid_coord.at<double>(0, 0)
		<< ", " << euclid_coord.at<double>(0, 1) << ", " << euclid_coord.at<double>(0, 2) << std::endl;
}
*/