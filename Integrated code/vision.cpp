// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include <stdio.h>
#include <cassert>
#include <ctime>
#include <iostream>

#include "vision.h"

#include "camera.h"
#include "unet_segmentation.h"
#include "points_matching_surf.h"
#include "thinning_zs.h"
#include "explore_skeleton.h"
#include "branch.h"
#include "branch3d.h"
#include "compute_circumferences_points.h"
#include "3d_visualization.h"

// Variables for sliders 
cv::Mat original;
cv::Mat morphological_image;
const int switch_slider_max = 1;
const int opening_slider_max = 50;
const int closing_slider_max = 50;
int switch_slider;
int opening_slider;
int closing_slider;
int switch_value;
int opening_size;
int closing_size;
bool apply_morphological_operations = false;
cv::Mat element_o;
cv::Mat element_c;

void cloudComputation(std::list<cv::Point3d>& all_points_old, 
	                  cv::Mat& all_centers_old, bool& written_cloud) {
	cv::Mat all_centers_current;
	std::list<cv::Point3d> all_points_current;
	cv::Mat warp_matrix;
	printTipTriangulation();
	initialization(&warp_matrix);
	while (true) {
		// int t1 = clock();
		reconstruct(warp_matrix, &all_points_current, &all_centers_current);
		all_centers_old = all_centers_current;
		all_points_old = all_points_current;
		written_cloud = true;
		// int t2 = clock();
		// std::cout << (t2 - t1) / static_cast<double>(CLOCKS_PER_SEC) << std::endl;
	}
}

void initialization(cv::Mat* warp_matrix) {
	// testReconstruction();
	initializeNeuralNetwork();
	openCameras();
	cv::Mat image_left, image_right;
	takeBothPictures(&image_left, &image_right);
	if (!image_left.empty() && !image_right.empty()) {
		*warp_matrix = warpMatrix(image_left, image_right);
		original = segment(image_left);
		openingAndClosingInterface();
		if (opening_slider == 0, closing_slider == 0) {
			apply_morphological_operations = false;
		}
	}
}

void reconstruct(cv::Mat& warp_matrix, std::list<cv::Point3d>* cloud_of_points,
	             cv::Mat* all_centers) {
	cv::Mat image_left = takeLeftPicture();
	if (!image_left.empty()) {
		cv::Mat image_left_seg = segment(image_left);
		image_left_seg = morphologicalOperations(image_left_seg);
		std::list<cv::Point3d> points;
		cv::Mat centers;
		structure3dReconstruction(image_left_seg, warp_matrix, &points, &centers);
		*cloud_of_points = points;
		*all_centers = centers;
		// cv::imshow("segmented", image_left_seg);
		// cv::waitKey(1000);
		// showCloud(*cloud_of_points);
		// std::cout << "number of points: " << cloud_of_points.size() << std::endl;
	}
}

void structure3dReconstruction(cv::Mat& img_seg_l, cv::Mat warp_m,
	                           std::list<cv::Point3d>* all_points,
	                           cv::Mat* all_centers) {
	cv::Mat vessels_centers_L, vessels_centers_L_mod, vessels_radii_L;
	thinning(img_seg_l, vessels_centers_L);
	cv::Rect myROI(1, 1, vessels_centers_L.cols - 2, vessels_centers_L.rows - 2);
	vessels_centers_L(myROI).copyTo(vessels_centers_L_mod);
	cv::copyMakeBorder(vessels_centers_L_mod, vessels_centers_L_mod, 1, 1, 1, 1, cv::BORDER_CONSTANT);
	// cv::imwrite("scheletro.png", vessels_centers_L);

	cv::distanceTransform(img_seg_l, vessels_radii_L, CV_DIST_L2, 3);
	std::list<Branch> branchesOfAllTrees =
		exploreSkeleton(vessels_centers_L_mod, vessels_radii_L);
	cv::Mat fake_center = (cv::Mat_<double>(1, 3) << 0, 0, 0);
	cv::Mat all_centers_plus_one = (cv::Mat_<double>(1, 3) << 0, 0, 0);
	for (const auto& element : branchesOfAllTrees) {
		Branch3d currentBranch(element, warp_m);
		if (currentBranch.getCentersCoord3d().rows > 5) {
			all_points->splice(all_points->end(),
				findAllCrfsOfBranchMean3(currentBranch));
			cv::Mat ctrs = currentBranch.getCentersCoord3d();
			cv::vconcat(all_centers_plus_one, ctrs.rowRange(1, ctrs.rows - 1),
				all_centers_plus_one);
			if (ctrs.rows % 2 != 0) {
				cv::vconcat(all_centers_plus_one, fake_center, all_centers_plus_one);
			}
		}
	}
	*all_centers = all_centers_plus_one.rowRange(1, all_centers_plus_one.rows);
}

void openingAndClosingInterface() {
	switch_slider = 0;
	opening_slider = 1;
	closing_slider = 1;
	apply_morphological_operations = true;
	original.copyTo(morphological_image);
	cv::namedWindow("Morphological Operations", cv::WINDOW_AUTOSIZE);
	cv::createTrackbar("Opening", "Morphological Operations",
		&opening_slider, opening_slider_max, changeOpeningAndClosing);
	cv::createTrackbar("Closing", "Morphological Operations",
		&closing_slider, closing_slider_max, changeOpeningAndClosing);
	cv::createTrackbar("Invert order", "Morphological Operations",
		&switch_slider, switch_slider_max, changeOpeningAndClosing);
	changeOpeningAndClosing(opening_slider, 0);
	cv::waitKey(0);
	cv::destroyWindow("Morphological Operations");
}

void changeOpeningAndClosing(int, void*) {
	switch_value = switch_slider;
	opening_size = opening_slider;
	closing_size = closing_slider;
	element_o = getStructuringElement(cv::MORPH_ELLIPSE,
		                              cv::Size(opening_size, opening_size));
	element_c = getStructuringElement(cv::MORPH_ELLIPSE,
		                              cv::Size(closing_size, closing_size));
	morphological_image = morphologicalOperations(original);
	cv::imshow("Morphological Operations", morphological_image);
}

cv::Mat morphologicalOperations(cv::Mat& pre) {
	cv::Mat post;
	if (apply_morphological_operations) {
		if (switch_value = 0) {
			cv::morphologyEx(pre, post, cv::MORPH_OPEN, element_o);
			cv::morphologyEx(post, post, cv::MORPH_CLOSE, element_c);
		} else {
			cv::morphologyEx(pre, post, cv::MORPH_CLOSE, element_c);
			cv::morphologyEx(post, post, cv::MORPH_OPEN, element_o);
		}
	} else {
		post = pre;
	}
	return post;
}

void printTipTriangulation() {
	cv::Mat LeftPos =
		(cv::Mat_<double>(4, 3) <<
			-7.968005073944982e-004, 2.126973412202958e-002, 8.001691002781067e-007,
			1.930995309766740e-002, -5.377383702374536e-004, 6.259457681897082e-007,
			1.026938985185630e-002, 8.616936963490442e-004, -9.196399628116005e-007,
			8.406325874842441e+002, -1.639259963849522e+002, 5.232451347040751e-001);
	cv::Mat RightPos =
		(cv::Mat_<double>(4, 3) <<
			-2.662505564483502e-003, -2.076866249013457e-002, -3.571167002404162e-007,
			-1.956092575998925e-002, 4.043862007490080e-003, -1.911425568323275e-007,
			-9.513348876575628e-003, 9.522048530861824e-005, -1.018423003963181e-006,
			-8.072916987952386e+002, 2.472202441520453e+002, -6.540305224564533e-001);


	cv::Point2d Lc(224, 287);
	cv::Point2d Rc(372, 250);
	cv::Mat absolute_4d_coord;

	// triangulation
	cv::triangulatePoints(
		LeftPos.t(), RightPos.t(), cv::Mat(Lc),
		cv::Mat(Rc), absolute_4d_coord);
	cv::Mat homog_coord = absolute_4d_coord.t();
	cv::Mat euclid_coord;
	cv::convertPointsFromHomogeneous(homog_coord.reshape(4), euclid_coord);
	std::cout << "vision -> triangulation of the tip: " << euclid_coord.at<double>(0, 0)
		<< ", " << euclid_coord.at<double>(0, 1) << ", " << euclid_coord.at<double>(0, 2) << std::endl;
}

void testReconstruction() {
	cv::Mat imgL =
		imread("left10.jpg", cv::IMREAD_COLOR);
	cv::Mat imgSegL =
		imread("left10s.jpg", cv::IMREAD_GRAYSCALE);
	cv::threshold(imgSegL, imgSegL, 125, 255, CV_THRESH_BINARY);
	cv::Mat imgR =
		imread("right10.jpg", cv::IMREAD_COLOR);

	cv::Mat warp_matrix = warpMatrix(imgL, imgR);

	int t1 = clock();

	std::list<cv::Point3d> cloud_of_points;
	cv::Mat all_centers;
	structure3dReconstruction(imgSegL, warp_matrix, &cloud_of_points,
		&all_centers);

	int t2 = clock();
	std::cout << "time SIMO 3D reconstruction: "
		<< (t2 - t1) / static_cast<double>(CLOCKS_PER_SEC) << std::endl;

	std::ofstream out("cloud10.txt");
	for (const auto& point : cloud_of_points) {
		out << point;
	}
	out << "end";
	out.close();
	std::cout << "cloud saved";
	showCloud(cloud_of_points);
}
