// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#ifndef SRC_SEGMENTATION_UNET_SEGMENTATION_H_
#define SRC_SEGMENTATION_UNET_SEGMENTATION_H_

#define BOOST_LIB_DIAGNOSTIC

#include <caffe/caffe.hpp>
#include <opencv2/opencv.hpp>
#include <vector>

void initializeNeuralNetwork();
cv::Mat segment(cv::Mat& src_img);
void wrapInputLayer(std::vector<cv::Mat>* input_channels,
	                caffe::shared_ptr<caffe::Net<float>> net);
void preprocess(const cv::Mat& img,
	            std::vector<cv::Mat>* input_channels);
#endif // SRC_SEGMENTATION_UNET_SEGMENTATION_H_
