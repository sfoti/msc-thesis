// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include "explore_skeleton.h"

std::list<Branch> exploreSkeleton(cv::Mat skel, cv::Mat vesselsRad) {
	std::list<Branch> branchesAllTrees;
	int n = 1;  // branch number
	for (int r = 0; r < skel.rows; r++) {
		for (int c = 0; c < skel.cols; c++) {
			if (skel.at<uchar>(r, c) == 255) {
				branchesAllTrees.splice(branchesAllTrees.end(),
					vesselsTree(r, c, n, &n, skel, vesselsRad));
			}
		}
	}
	// std::cout << "explore_skeleton -> number of total branches: " << n - 1 << std::endl;
	return branchesAllTrees;
}

std::list<Branch> vesselsTree(int r, int c, int n, int* nNew, cv::Mat sk,
	cv::Mat R) {
	std::list<Branch> explored;
	std::list<cv::Point2i> unexplored;
	unexplored.push_back(cv::Point(c, r));
	while (!unexplored.empty()) {
		Branch newBranch(unexplored.front().y, unexplored.front().x, n, sk, R);
		explored.push_back(newBranch);
		unexplored.pop_front();
		unexplored.splice(unexplored.end(), newBranch.getNewCentersCoord());
		n++;
	}
	*nNew = n;
	return explored;
}

void showColoredTree(cv::Mat img, std::list<Branch> all_branches, int window) {
	const int num_branches = all_branches.size();
	cv::Mat img_colorized(img.rows, img.cols, CV_8UC3, cv::Vec3b(0, 255, 255));
	for (int r = 0; r < img.rows; ++r) {
		for (int c = 0; c < img.cols; ++c) {
			const uchar original_color = img.at<uchar>(r, c);
			const uchar hue = static_cast<int>(255.0 * original_color / num_branches);
			assert(hue >= 0 && hue < 256);
			img_colorized.at<cv::Vec3b>(r, c)[0] = hue;
			if (original_color == 0) {
				img_colorized.at<cv::Vec3b>(r, c)[2] = 0;
			}
		}
	}
	cv::cvtColor(img_colorized, img_colorized, CV_HSV2BGR);
	std::stringstream msg;
	msg << "colored image " << window;
	cv::imshow(msg.str(), img_colorized);
	cv::waitKey();
}