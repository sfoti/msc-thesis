// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#ifndef SRC_TIP_POSITION_AND_CONTROL_VISION_AND_CONTROL_H_
#define SRC_TIP_POSITION_AND_CONTROL_VISION_AND_CONTROL_H_

//#define GLOG_NO_ABBREVIATED_SEVERITIES
//#define NO_STRICT

#include <list>
#include <vector>
#include <opencv2/opencv.hpp>

void controlMicronWithVision();
cv::Point3d computeGoalPosition(double dist, cv::Point3d versor,
	cv::Point3d tip);
void waitConnection();

#endif  // SRC_TIP_POSITION_AND_CONTROL_VISION_AND_CONTROL_H_
