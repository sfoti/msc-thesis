// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include "unet_segmentation.h"

#include <caffe/layers/input_layer.hpp>
#include <caffe/layers/deconv_layer.hpp>
#include <caffe/layers/concat_layer.hpp>

#include <ctime>

namespace caffe {
	extern INSTANTIATE_CLASS(InputLayer);
	extern INSTANTIATE_CLASS(DeconvolutionLayer);
	extern INSTANTIATE_CLASS(ConcatLayer);
}

caffe::shared_ptr<caffe::Net<float>> unet;
cv::Size original_size = cv::Size(800, 608);
cv::Size network_size = cv::Size(288, 224);

void initializeNeuralNetwork() {
	caffe::Caffe::SetDevice(0);
	caffe::Caffe::set_mode(caffe::Caffe::GPU);
	// opencv opens images in BGR as default

	//get the net
	unet.reset(new caffe::Net<float>("deploy3.prototxt", caffe::TEST));
	//get trained net
	unet->CopyTrainedLayersFrom("unet_iter_15840.caffemodel");	
}

cv::Mat segment(cv::Mat& src_img) {

	std::vector<cv::Mat> input_channels;
	wrapInputLayer(&input_channels, unet);
	preprocess(src_img, &input_channels);
	
	// int t1 = clock();

	const std::vector<caffe::Blob<float>*>& result = unet->Forward();

	// int t2 = clock();
	// std::cout << (t2 - t1) / static_cast<double>(CLOCKS_PER_SEC) << std::endl;

	caffe::Blob<float>* input_layer = unet->output_blobs()[0];
	int width = input_layer->width();
	int height = input_layer->height();
	int channels = input_layer->channels();
	cv::Mat out_img(height, width, CV_8UC1);
	// Copy the output layer to a std::vector 
	const float* result_vec = result[0]->cpu_data();

	int index = 0;
	cv::Mat class_each_row(channels, width*height, CV_32FC1);
	for (int i = 0; i < channels; i++) {
		for (int j = 0; j < width * height; j++) {
			class_each_row.at<float>(i, j) = result_vec[index];
			index++;
		}
	}
	class_each_row = class_each_row.t();
	cv::Point maxId;
	double maxValue;
	for (int i = 0; i<class_each_row.rows; i++) {
		cv::minMaxLoc(class_each_row.row(i), 0, &maxValue, 0, &maxId);
		out_img.at<uchar>(i) = maxId.x;
	}
	cv::resize(out_img, out_img, original_size);
	return out_img * 255;
}

void wrapInputLayer(std::vector<cv::Mat>* input_channels,
	                caffe::shared_ptr<caffe::Net<float>> net) {
	caffe::Blob<float>* input_layer = net->input_blobs()[0];
	int width = input_layer->width();
	int height = input_layer->height();
	float* input_data = input_layer->mutable_cpu_data();
	for (int i = 0; i < input_layer->channels(); ++i) {
		cv::Mat channel(height, width, CV_32FC1, input_data);
		input_channels->push_back(channel);
		input_data += width * height;
	}
}

void preprocess(const cv::Mat& img,
	            std::vector<cv::Mat>* input_channels) {
	cv::Mat img_RGB;
	cv::Mat img_float;
	cv::cvtColor(img, img_RGB, cv::COLOR_BGR2RGB);
	cv::resize(img_RGB, img_RGB, network_size);
	img_RGB.convertTo(img_float, CV_32FC3);
	/// This operation will write the separate BGR planes directly to the
	// input layer of the network because it is wrapped by the cv::Mat
	// objects in input_channels. 
	cv::split(img_float, *input_channels);
}
