// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#ifndef SRC_VESSEL_GRAPH_VESSEL_GRAPH_BUILDER_H_
#define SRC_VESSEL_GRAPH_VESSEL_GRAPH_BUILDER_H_

#include <ctime>
#include <fstream>
#include <list>
#include <map>
#include <vector>

#include <opencv2/opencv.hpp>

struct Pixel {
	int r, c;
};
struct Vessel {
	std::vector<Pixel> pixels;
};

void ConstructVesselGraph(const Pixel current_pixel, const int vessel_id,
	cv::Mat* vessels_img, std::vector<Vessel>* vessels);
void ConstructVesselGraph(cv::Mat* vessels_img);

#endif  // SRC_VESSEL_GRAPH_VESSEL_GRAPH_BUILDER_H_