// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include "src/vessel_graph/vessel_graph_builder.h"

void ConstructVesselGraph(const Pixel current_pixel, const int vessel_id,
                          cv::Mat* vessels_img, std::vector<Vessel>* vessels) {
  const int num_rows = vessels_img->rows;
  const int num_cols = vessels_img->cols;

  assert(vessel_id <= 200);
  const uchar kWHITE = 255;
  const int dr_main[4] = {-1, 0, 0, 1};
  const int dc_main[4] = {0, -1, 1, 0};

  const int dr_diag[4] = {-1, -1, 1, 1};
  const int dc_diag[4] = {-1, 1, -1, 1};

  const int row_id = current_pixel.r;
  const int col_id = current_pixel.c;
  assert(vessels_img->at<uchar>(row_id, col_id) == kWHITE);

  vessels->at(vessel_id).pixels.push_back(current_pixel);
  vessels_img->at<uchar>(row_id, col_id) = vessel_id;

  int n_adjacent = 0;
  for (int i = 0; i < 4; ++i) {
    const int adj_row_id = row_id + dr_main[i];
    const int adj_col_id = col_id + dc_main[i];

    if (adj_row_id >= 0 && adj_row_id < num_rows && adj_col_id >= 0 &&
        adj_col_id < num_cols) {
      if (vessels_img->at<uchar>(adj_row_id, adj_col_id) == kWHITE) {
        ++n_adjacent;
      }
    }
  }
  for (int i = 0; i < sizeof(dr_diag) / sizeof(int); ++i) {
    const int adj_row_id = row_id + dr_diag[i];
    const int adj_col_id = col_id + dc_diag[i];

    if (adj_row_id >= 0 && adj_row_id < num_rows && adj_col_id >= 0 &&
        adj_col_id < num_cols) {
      bool adj_is_white =
          vessels_img->at<uchar>(adj_row_id, adj_col_id) == kWHITE;
      bool x1_is_white = vessels_img->at<uchar>(row_id, adj_col_id) == kWHITE;
      bool x2_is_white = vessels_img->at<uchar>(adj_row_id, col_id) == kWHITE;
      if (adj_is_white && !x1_is_white && !x2_is_white) {
        ++n_adjacent;
      }
    }
  }

  // std::cout << "Pixel (" << row_id << ", " << col_id << ") n_adj = "
  //          << n_adjacent << std::endl;

  if (n_adjacent <= 1) {
    for (int i = 0; i < 4; ++i) {
      const int adj_row_id = row_id + dr_main[i];
      const int adj_col_id = col_id + dc_main[i];

      if (adj_row_id >= 0 && adj_row_id < num_rows && adj_col_id >= 0 &&
          adj_col_id < num_cols) {
        if (vessels_img->at<uchar>(adj_row_id, adj_col_id) == kWHITE) {
          ConstructVesselGraph({adj_row_id, adj_col_id}, vessel_id, vessels_img,
                               vessels);
        }
      }
    }
    for (int i = 0; i < 4; ++i) {
      const int adj_row_id = row_id + dr_diag[i];
      const int adj_col_id = col_id + dc_diag[i];

      if (adj_row_id >= 0 && adj_row_id < num_rows && adj_col_id >= 0 &&
          adj_col_id < num_cols) {
        if (vessels_img->at<uchar>(adj_row_id, adj_col_id) == kWHITE) {
          ConstructVesselGraph({adj_row_id, adj_col_id}, vessel_id, vessels_img,
                               vessels);
        }
      }
    }
  } else {
    for (int i = 0; i < 4; ++i) {
      const int adj_row_id = row_id + dr_main[i];
      const int adj_col_id = col_id + dc_main[i];

      if (adj_row_id >= 0 && adj_row_id < num_rows && adj_col_id >= 0 &&
          adj_col_id < num_cols) {
        if (vessels_img->at<uchar>(adj_row_id, adj_col_id) == kWHITE) {
          int new_vessel_id = vessels->size();
          vessels->emplace_back();
          ConstructVesselGraph({adj_row_id, adj_col_id}, new_vessel_id,
                               vessels_img, vessels);
        }
      }
    }
    for (int i = 0; i < 4; ++i) {
      const int adj_row_id = row_id + dr_diag[i];
      const int adj_col_id = col_id + dc_diag[i];

      if (adj_row_id >= 0 && adj_row_id < num_rows && adj_col_id >= 0 &&
          adj_col_id < num_cols) {
        if (vessels_img->at<uchar>(adj_row_id, adj_col_id) == kWHITE) {
          int new_vessel_id = vessels->size();
          vessels->emplace_back();
          ConstructVesselGraph({adj_row_id, adj_col_id}, new_vessel_id,
                               vessels_img, vessels);
        }
      }
    }
  }
}

void ConstructVesselGraph(cv::Mat* vessels_img) {
  const int num_rows = vessels_img->rows;
  const int num_cols = vessels_img->cols;

  const uchar kWHITE = 255;
  std::vector<Vessel> vessels;

  for (int row_id = 0; row_id < num_rows; ++row_id) {
    for (int col_id = 0; col_id < num_cols; ++col_id) {
      if (vessels_img->at<uchar>(row_id, col_id) == kWHITE) {
        int vessel_id = vessels.size();
        vessels.emplace_back();
        ConstructVesselGraph({row_id, col_id}, vessel_id, vessels_img,
                             &vessels);
      }
    }
  }

  std::cout << "Found " << vessels.size() << " vessels." << std::endl;
}
