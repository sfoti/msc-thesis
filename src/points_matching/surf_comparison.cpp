// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include <algorithm>
#include <iostream>
#include "src/points_matching/points_matching_surf.h"
#include "src/points_matching/surf_comparison.h"

void comparisonWithSURF(cv::Mat img_left, cv::Mat img_right,
                        cv::Mat img_seg_left) {
  std::vector<cv::Point2i> left_points;
  std::vector<cv::Point2i> right_points;
  matchedPoints(img_left, img_right, &left_points, &right_points);
  std::vector<cv::Point2d> points_left_structure;
  std::vector<cv::Point2d> points_right_structure;
  for (int i = 0; i < static_cast<int>(left_points.size()); i++) {
    if (static_cast<int>(img_seg_left.at<uchar>(left_points[i])) > 0) {
      points_left_structure.push_back(left_points[i]);
      points_right_structure.push_back(right_points[i]);
    }
  }
  cv::Mat points_3d =
      find3dPoints(points_left_structure, points_right_structure);
  std::cout << "SURF cloud size: " << points_3d.rows << std::endl;
  // showCloud(points_3d);
}

cv::Mat find3dPoints(std::vector<cv::Point2d> coord_left,
                     std::vector<cv::Point2d> coord_right) {
  cv::Mat LeftPos =
      (cv::Mat_<double>(4, 3) << -4.266623151908218e-003,
       2.282481250894210e-002, -7.692827741277093e-006, 1.855814183492632e-002,
       -3.583447918429969e-003, -4.115888935727753e-006, 1.154676368879985e-002,
       8.585981167283711e-003, 6.722213779405889e-006, 1.391072755153458e+003,
       4.826085439366972e+002, 1.011843689328007e+000);
  cv::Mat RightPos =
      (cv::Mat_<double>(4, 3) << 4.724351553831909e-004, 2.224810582116892e-002,
       -8.183688143342833e-006, 1.870910921163596e-002, -7.612902047805994e-003,
       -4.015670721251107e-006, 1.114012708472058e-002, 7.073057481874910e-003,
       7.167188770153057e-006, 1.368029368392658e+003, 2.837603111041603e+002,
       1.039408195870466e+000);

  cv::Mat coord_left_mat = cv::Mat(coord_left);
  coord_left_mat.convertTo(coord_left_mat, CV_64F);
  cv::Mat centersHomogeneous, centersEuclidean;
  cv::triangulatePoints(LeftPos.t(), RightPos.t(), coord_left_mat,
                        cv::Mat(coord_right),
                        centersHomogeneous);  // transpose .t() invert .inv()
  cv::Mat cH = centersHomogeneous.t();
  cv::convertPointsFromHomogeneous(cH.reshape(4), centersEuclidean);
  return centersEuclidean;
}
