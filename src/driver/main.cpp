// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include <stdio.h>
#include <cassert>
#include <ctime>
#include <iostream>
#include <opencv2/opencv.hpp>

#include "src/3d_reconstruction/3d_visualization.h"
#include "src/3d_reconstruction/branch3d.h"
#include "src/3d_reconstruction/compute_circumferences_points.h"
#include "src/points_matching/points_matching_surf.h"
#include "src/segmentation/thresholding_segmentation.h"
#include "src/third_party/thinning_zs.h"
#include "src/tip_position/estimate_distance_tip_structure.h"
#include "src/tracking/tracking.h"
#include "src/vessel_graph/branch.h"
#include "src/vessel_graph/explore_skeleton.h"
#include "src/vessel_graph/vessel_graph_builder.h"

void structure3dReconstruction(cv::Mat img_seg_l, cv::Mat warp_m,
                               std::list<cv::Point3d>* all_points,
                               cv::Mat* all_centers);
void comparisonWithSURF(cv::Mat img_left, cv::Mat img_right,
                        cv::Mat img_seg_left);
cv::Mat find3dPoints(std::vector<cv::Point2d> coord_left,
                     std::vector<cv::Point2d> coord_right);
int main() {
  // Img abdomen vessels
  cv::Mat imgL =
      imread("/home/simo/Immagini/imgTesi/sx1.png", cv::IMREAD_GRAYSCALE);
  cv::Mat imgSegL =
      imread("/home/simo/Immagini/imgTesi/sx1seg.png", cv::IMREAD_GRAYSCALE);
  // per essere sicuri di non aver lasciato pixel grigi in segmentazione manuale
  cv::threshold(imgSegL, imgSegL, 125, 255, CV_THRESH_BINARY);
  cv::Mat imgR =
      imread("/home/simo/Immagini/imgTesi/dx1.png", cv::IMREAD_GRAYSCALE);

  /*
  // MANUAL SEGMENTATION
  cv::Mat v, n;
  cv::Mat imgSegThTest =
      imread("/home/simo/Immagini/imgTesi/acquisition28Aug/left1.png",
             cv::IMREAD_COLOR);
  segmetStructures(imgSegThTest, &v, &n);

  cv::imshow("vessels", v);
  cv::imshow("nerves", n);
  cv::waitKey(0);
  */

  cv::Mat warp_matrix = warpMatrix(imgL, imgR);
  int t1 = clock();

  std::list<cv::Point3d> cloud_of_points;
  cv::Mat all_centers;
  structure3dReconstruction(imgSegL, warp_matrix, &cloud_of_points,
                            &all_centers);
  showCloud(cloud_of_points);

  int t2 = clock();
  std::cout << "time SIMO 3D reconstruction: "
            << (t2 - t1) / static_cast<double>(CLOCKS_PER_SEC) << std::endl;

  std::cout << "number of centers: " << all_centers.rows << std::endl;
  std::cout << "number of points: " << cloud_of_points.size() << std::endl;
  int t3 = clock();
  cv::Point3d t_pos(0, 0, 0);
  std::cout << "distance structure to origin: "
            << estimateTipDistance(t_pos, all_centers, cloud_of_points) << " mm"
            << std::endl;
  int t4 = clock();
  std::cout << "time for distance computation: "
            << (t4 - t3) / static_cast<double>(CLOCKS_PER_SEC) << std::endl;
  /*
  // TRAKING PART
  cv::Mat imgL2 =
      imread("/home/simo/Immagini/imgTesi/sx1rot.png", cv::IMREAD_GRAYSCALE);
  trackTwoImages(imgL, imgL2, imgSegL);

  // VIDEO PART

  cv::VideoCapture cap(
      "/home/simo/Immagini/imgTesi/acquisition28Aug/leftVideo.avi");
  if (!cap.isOpened()) {
    std::cout << "Error opening video stream or file" << std::endl;
    return -1;
  }
  cv::Mat previous_frame, previous_segmentation;
  cv::Mat current_frame;
  cv::Mat current_segmentation =
      imread("/home/simo/Immagini/imgTesi/acquisition28Aug/leftSegFrame1.png",
             cv::IMREAD_GRAYSCALE);
  bool flag_first_frame = true;
  while (1) {
    previous_frame = current_frame;
    cap >> current_frame;
    if (current_frame.empty()) break;
    cv::cvtColor(current_frame, current_frame, cv::COLOR_BGR2GRAY);
    if (flag_first_frame == true) {
      flag_first_frame = false;
    } else {
      previous_segmentation = current_segmentation;
      current_segmentation =
          segmentNewFrame(previous_frame, current_frame, previous_segmentation);
      cv::imshow("Frame", current_segmentation);
    }
    char c = static_cast<char>(cv::waitKey(25));
    if (c == 27) break;
  }
  cap.release();
  cv::destroyAllWindows();

  // END VIDEO PART
  */

  int t5 = clock();
  comparisonWithSURF(imgL, imgR, imgSegL);
  int t6 = clock();
  std::cout << "time for SURF reconstruction: "
            << (t6 - t5) / static_cast<double>(CLOCKS_PER_SEC) << std::endl;

  std::cout << "fine" << std::endl;
  return 0;
}

void structure3dReconstruction(cv::Mat img_seg_l, cv::Mat warp_m,
                               std::list<cv::Point3d>* all_points,
                               cv::Mat* all_centers) {
  cv::Mat vessels_centers_L, vessels_radii_L;
  thinning(img_seg_l, vessels_centers_L);
  cv::distanceTransform(img_seg_l, vessels_radii_L, CV_DIST_L2, 3);
  std::list<Branch> branchesOfAllTrees =
      exploreSkeleton(vessels_centers_L, vessels_radii_L);
  cv::Mat fake_center = (cv::Mat_<double>(1, 3) << 0, 0, 0);
  cv::Mat all_centers_plus_one = (cv::Mat_<double>(1, 3) << 0, 0, 0);
  for (const auto& element : branchesOfAllTrees) {
    Branch3d currentBranch(element, warp_m);
    if (currentBranch.getCentersCoord3d().rows > 5) {
      all_points->splice(all_points->end(),
                         findAllCrfsOfBranchMean3(currentBranch));
      cv::Mat ctrs = currentBranch.getCentersCoord3d();
      cv::vconcat(all_centers_plus_one, ctrs.rowRange(1, ctrs.rows - 1),
                  all_centers_plus_one);
      if (ctrs.rows % 2 != 0) {
        cv::vconcat(all_centers_plus_one, fake_center, all_centers_plus_one);
      }
    }
  }
  *all_centers = all_centers_plus_one.rowRange(1, all_centers_plus_one.rows);
}
