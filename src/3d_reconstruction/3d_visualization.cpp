// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include "src/3d_reconstruction/3d_visualization.h"

#include <vector>

#include <opencv2/viz.hpp>

void showCloud(cv::Mat M) {
  cv::viz::Viz3d viz_elem("show_cloud");
  viz_elem.setBackgroundColor(cv::viz::Color::black());
  viz_elem.showWidget("coosys", cv::viz::WCoordinateSystem());
  viz_elem.showWidget("cloud", cv::viz::WCloud(M, cv::viz::Color::white()));
  viz_elem.spin();
}

void showCloud(std::list<cv::Point3d> list_of_points) {
  std::vector<cv::Point3d> vec_pts(list_of_points.begin(),
                                   list_of_points.end());
  cv::viz::Viz3d viz_elem("show_cloud");
  viz_elem.setBackgroundColor(cv::viz::Color::black());
  viz_elem.showWidget("coosys", cv::viz::WCoordinateSystem());
  viz_elem.showWidget(
      "cloud", cv::viz::WCloud(cv::Mat(vec_pts), cv::viz::Color::white()));
  viz_elem.spin();
}

/*
void showCloudAllBranches(std::list<Branch> all_branches, cv::Mat warp_matrix) {
  std::list<cv::Point3d> all_points;
  for (const auto& it : all_branches) {
    Branch3d currentBranch(it, warp_matrix);
    all_points.splice(all_points.end(),
                      findAllCrfsOfBranchMean3(currentBranch));
  }
  showCloud(all_points);
}
*/
