// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#ifndef SRC_3D_RECONSTRUCTION_COMPUTE_CIRCUMFERENCES_POINTS_H_
#define SRC_3D_RECONSTRUCTION_COMPUTE_CIRCUMFERENCES_POINTS_H_

#include <list>
#include <vector>

#include <opencv2/opencv.hpp>

#include "src/3d_reconstruction/branch3d.h"

std::list<cv::Point3d> findAllCrfsOfBranchMean3(Branch3d currentBr);
std::list<cv::Point3d> findAllCrfsOfBranchMean2(Branch3d currentBr);
std::list<cv::Point3d> findAllCrfsOfBranchNoMean(Branch3d currentBr);
std::list<cv::Point3d> computeCrfPts(cv::Point3d center,
                                     cv::Point3d normal_versor, double radius);
cv::Point3d tangentToCurve(cv::Point3d pre_center, cv::Point3d post_center);
cv::Point3d meanTangent(cv::Point3d first_versor, cv::Point3d second_versor,
                        cv::Point3d current_versor, int index);
cv::Point3d meanTangent(cv::Point3d prev_versor, cv::Point3d current_versor,
                        int index);

#endif  // SRC_3D_RECONSTRUCTION_COMPUTE_CIRCUMFERENCES_POINTS_H_
