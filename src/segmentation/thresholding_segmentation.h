// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#ifndef SRC_SEGMENTATION_THRESHOLDING_SEGMENTATION_H_
#define SRC_SEGMENTATION_THRESHOLDING_SEGMENTATION_H_

#include <stdio.h>
#include <iostream>
#include <string>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

void segmetStructures(cv::Mat src, cv::Mat* vessels, cv::Mat* nerves);
void contrastAndBrightnessInterface();
void segmentationInterface(int channel);
void changeContrastAndBrightness(int, void*);
void segmentStructures(int, void* userdata);
void showTwoImages(std::string nome_finestra, cv::Mat img_1, cv::Mat img_2);
void showTwoImagesWithTransparecy(std::string nome_finestra, cv::Mat img_1,
                                  cv::Mat img_1_back, cv::Mat img_2,
                                  cv::Mat img_2_back, int channel);
cv::Mat selectChannelColoredImg(cv::Mat src, int channel);

#endif  // SRC_SEGMENTATION_THRESHOLDING_SEGMENTATION_H_
