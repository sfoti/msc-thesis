// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include "src/segmentation/thresholding_segmentation.h"

cv::Mat src;
cv::Mat adjusted_image;
cv::Mat b, g, r;
cv::Mat vessels_b, vessels_g, vessels_r;
cv::Mat nerves_b, nerves_g, nerves_r;

const int alpha_slider_max = 100;
const int beta_slider_max = 100;
const int lower_vessels_threshold_slider_max = 255;
const int higher_vessels_threshold_slider_max = 255;
const int lower_nerves_threshold_slider_max = 255;
const int higher_nerves_threshold_slider_max = 255;
const int transparency_a_slider_max = 100;

int alpha_slider;
int beta_slider;
int lower_vessels_threshold_slider;
int higher_vessels_threshold_slider;
int lower_nerves_threshold_slider;
int higher_nerves_threshold_slider;
int transparency_a_slider;

double alpha;  // contrast
int beta;      // brightness
uchar lower_vessels_threshold;
uchar higher_vessels_threshold;
uchar lower_nerves_threshold;
uchar higher_nerves_threshold;
double transparency_a;
double transparency_b;

void segmetStructures(cv::Mat src_colour, cv::Mat* vessels, cv::Mat* nerves) {
  src = src_colour;
  contrastAndBrightnessInterface();
  // cv::cvtColor(adjusted_image, adjusted_image, cv::COLOR_BGR2HSV);
  b = selectChannelColoredImg(adjusted_image, 0);
  g = selectChannelColoredImg(adjusted_image, 1);
  r = selectChannelColoredImg(adjusted_image, 2);
  for (int ch = 0; ch < 3; ch++) {
    segmentationInterface(ch);
  }
  cv::bitwise_and(vessels_b, vessels_g, vessels_g);
  cv::bitwise_and(nerves_b, nerves_g, nerves_g);
  cv::bitwise_and(vessels_g, vessels_r, vessels_r);
  cv::bitwise_and(nerves_g, nerves_r, nerves_r);
  cv::Mat element = getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(7, 7));
  cv::morphologyEx(vessels_r, *vessels, cv::MORPH_OPEN, element);
  cv::morphologyEx(nerves_r, *nerves, cv::MORPH_OPEN, element);
}

void contrastAndBrightnessInterface() {
  alpha_slider = 0;
  beta_slider = 50;
  src.copyTo(adjusted_image);
  cv::namedWindow("Contrast and Brightness correction", cv::WINDOW_AUTOSIZE);
  cv::createTrackbar("Contrast", "Contrast and Brightness correction",
                     &alpha_slider, alpha_slider_max,
                     changeContrastAndBrightness);
  cv::createTrackbar("Brightness", "Contrast and Brightness correction",
                     &beta_slider, beta_slider_max,
                     changeContrastAndBrightness);
  changeContrastAndBrightness(alpha_slider, 0);
  cv::waitKey(0);
  cv::destroyWindow("Contrast and Brightness correction");
}

void segmentationInterface(int channel) {
  assert(channel < 3 && channel >= 0);
  lower_vessels_threshold_slider = 0;
  higher_vessels_threshold_slider = 10;
  lower_nerves_threshold_slider = 10;
  higher_nerves_threshold_slider = 255;
  transparency_a_slider = 10;
  if (channel == 0) {
    b.copyTo(vessels_b);
    b.copyTo(nerves_b);
  }
  if (channel == 1) {
    g.copyTo(vessels_g);
    g.copyTo(nerves_g);
  }
  if (channel == 2) {
    r.copyTo(vessels_r);
    r.copyTo(nerves_r);
  }
  std::string window_name = "channel " + std::to_string(channel) +
                            ".   (Left side = vessels. Right side = nerves)";
  segmentStructures(lower_vessels_threshold_slider,
                    reinterpret_cast<void*>(&channel));
  cv::namedWindow(window_name, cv::WINDOW_AUTOSIZE);
  cv::createTrackbar("Overlap Original Image", window_name,
                     &transparency_a_slider, transparency_a_slider_max,
                     segmentStructures, reinterpret_cast<void*>(&channel));
  cv::createTrackbar("Low threshold Vessels", window_name,
                     &lower_vessels_threshold_slider,
                     lower_vessels_threshold_slider_max, segmentStructures,
                     reinterpret_cast<void*>(&channel));
  cv::createTrackbar("High threshold Vessels", window_name,
                     &higher_vessels_threshold_slider,
                     higher_vessels_threshold_slider_max, segmentStructures,
                     reinterpret_cast<void*>(&channel));
  cv::createTrackbar("Low threshold Nerves", window_name,
                     &lower_nerves_threshold_slider,
                     lower_nerves_threshold_slider_max, segmentStructures,
                     reinterpret_cast<void*>(&channel));
  cv::createTrackbar("High threshold Nerves", window_name,
                     &higher_nerves_threshold_slider,
                     higher_nerves_threshold_slider_max, segmentStructures,
                     reinterpret_cast<void*>(&channel));
  cv::waitKey(0);
  cv::destroyWindow(window_name);
}

void changeContrastAndBrightness(int, void*) {
  // alpha = contrast [1.0, 3.0]
  // beta = brightness [0, 100]
  alpha = 1 + static_cast<double>(alpha_slider) * 2 / 100;
  beta = (beta_slider - 50) * 2;
  for (int y = 0; y < src.rows; y++) {
    for (int x = 0; x < src.cols; x++) {
      for (int c = 0; c < 3; c++) {
        adjusted_image.at<cv::Vec3b>(y, x)[c] = cv::saturate_cast<uchar>(
            alpha * (src.at<cv::Vec3b>(y, x)[c]) + beta);
      }
    }
  }
  cv::GaussianBlur(adjusted_image, adjusted_image, cv::Size(17, 17), 0, 0);
  cv::imshow("Contrast and Brightness correction", adjusted_image);
}

void segmentStructures(int, void* userdata) {
  cv::Mat img_in, img_out_vessel, img_out_nerve;
  std::string window_name;
  int ch = *(static_cast<int*>(userdata));
  if (ch == 0) {
    img_in = b;
    img_out_vessel = vessels_b;
    img_out_nerve = nerves_b;
    window_name = "channel 0.   (Left side = vessels. Right side = nerves)";
  }
  if (ch == 1) {
    img_in = g;
    img_out_vessel = vessels_g;
    img_out_nerve = nerves_g;
    window_name = "channel 1.   (Left side = vessels. Right side = nerves)";
  }
  if (ch == 2) {
    img_in = r;
    img_out_vessel = vessels_r;
    img_out_nerve = nerves_r;
    window_name = "channel 2.   (Left side = vessels. Right side = nerves)";
  }
  uchar lower_vessels_threshold =
      static_cast<uchar>(lower_vessels_threshold_slider);
  uchar higher_vessels_threshold =
      static_cast<uchar>(higher_vessels_threshold_slider);
  uchar lower_nerves_threshold =
      static_cast<uchar>(lower_nerves_threshold_slider);
  uchar higher_nerves_threshold =
      static_cast<uchar>(higher_nerves_threshold_slider);
  cv::Mat img_in_th;
  cv::threshold(img_in, img_in_th, lower_vessels_threshold, 0, 3);
  cv::inRange(img_in_th, lower_vessels_threshold, higher_vessels_threshold,
              img_out_vessel);
  cv::inRange(img_in_th, lower_nerves_threshold, higher_nerves_threshold,
              img_out_nerve);

  transparency_a =
      static_cast<double>(transparency_a_slider) / transparency_a_slider_max;
  transparency_b = (1.0 - transparency_a);
  showTwoImagesWithTransparecy(window_name, img_out_vessel, adjusted_image,
                               img_out_nerve, adjusted_image, ch);
}

void showTwoImages(std::string nome_finestra, cv::Mat img_1, cv::Mat img_2) {
  cv::Mat imgs_concat;
  cv::hconcat(img_1, img_2, imgs_concat);
  cv::imshow(nome_finestra, imgs_concat);
}

void showTwoImagesWithTransparecy(std::string nome_finestra, cv::Mat img_1,
                                  cv::Mat img_1_back, cv::Mat img_2,
                                  cv::Mat img_2_back, int channel) {
  cv::Mat imgs_concat;
  cv::Mat imgs_concat_back;
  cv::Mat dst;
  img_1_back = selectChannelColoredImg(img_1_back, channel);
  img_2_back = selectChannelColoredImg(img_2_back, channel);
  cv::hconcat(img_1, img_2, imgs_concat);
  cv::hconcat(img_1_back, img_2_back, imgs_concat_back);
  cv::addWeighted(imgs_concat, transparency_a, imgs_concat_back, transparency_b,
                  0.0, dst);
  cv::imshow(nome_finestra, dst);
}

cv::Mat selectChannelColoredImg(cv::Mat src, int channel) {
  assert(channel < 3 && channel >= 0);
  cv::Mat bgr[3];       // destination array
  cv::split(src, bgr);  // split source
  return bgr[channel];
}
