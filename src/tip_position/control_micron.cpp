// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include "src/tip_position/control_micron.h"
#include "src/tip_position/estimate_distance_tip_structure.h"

const double SAFETY_DISTANCE = 1.5;  // in mm

cv::Point3d controlMicron(cv::Point3d tip_position, cv::Mat all_centers,
                          std::list<cv::Point3d> all_points) {
  double distance_tip_cloud;
  cv::Point3d versor_point_to_tip;
  estimateTipDistanceAndNormal(tip_position, all_centers, all_points,
                               &distance_tip_cloud, &versor_point_to_tip);
  if (distance_tip_cloud <= SAFETY_DISTANCE) {
    computeGoalPosition(distance_tip_cloud, versor_point_to_tip, tip_position);
    // sendGoalPositionToMicron(
    //    computeGoalPosition(distance_tip_cloud, versor_point_to_tip));
  }
}

cv::Point3d computeGoalPosition(double dist, cv::Point3d versor,
                                cv::Point3d tip) {
  return (((SAFETY_DISTANCE * 1000 - dist) * versor) + tip);  // in um
}
