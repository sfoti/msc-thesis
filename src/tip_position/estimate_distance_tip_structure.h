// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>
#ifndef SRC_TIP_POSITION_ESTIMATE_DISTANCE_TIP_STRUCTURE_H_
#define SRC_TIP_POSITION_ESTIMATE_DISTANCE_TIP_STRUCTURE_H_

#include <list>
#include <opencv2/opencv.hpp>

void estimateTipDistanceAndNormal(cv::Point3d tip_position, cv::Mat all_centers,
                                  std::list<cv::Point3d> all_points,
                                  double* min_distance,
                                  cv::Point3d* versor_point_to_tip);
double estimateTipDistance(cv::Point3d tip_position, cv::Mat all_centers,
                           std::list<cv::Point3d> all_points);
int findClosestCenterIndex(cv::Point3d tip_position, cv::Mat all_centers);

#endif  // SRC_TIP_POSITION_ESTIMATE_DISTANCE_TIP_STRUCTURE_H_
