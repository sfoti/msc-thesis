// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#include <iostream>
#include "src/tracking/tracking.h"

cv::Mat segmentNewFrame(cv::Mat prev_frame, cv::Mat following_frame,
                        cv::Mat prev_segmentation) {
  cv::Mat flow;
  cv::calcOpticalFlowFarneback(prev_frame, following_frame, flow, 0.5, 3, 15, 3,
                               7, 1.2, 0);
  return drawNewSegmentedImage(flow, prev_segmentation);
}

void trackTwoImages(cv::Mat prev_frame, cv::Mat following_frame,
                    cv::Mat prev_segmentation) {
  cv::Mat flow;
  cv::calcOpticalFlowFarneback(prev_frame, following_frame, flow, 0.5, 3, 15, 3,
                               7, 1.2, 0);
  // cv::Mat cflow = drawOptFlowMap(flow, prev_frame, 16, 1.5);
  cv::Mat new_segmentation = drawNewSegmentedImage(flow, prev_segmentation);
  // cv::imshow("flow", cflow);
  cv::imshow("new frame", new_segmentation);
  cv::waitKey(0);
}

cv::Mat drawOptFlowMap(cv::Mat in, cv::Mat prev, int step, double) {
  cv::Mat cflowmap;
  cv::cvtColor(prev, cflowmap, cv::COLOR_GRAY2BGR);
  for (int y = 0; y < cflowmap.rows; y += step) {
    for (int x = 0; x < cflowmap.cols; x += step) {
      const cv::Point2f fxy = in.at<cv::Point2f>(y, x);
      cv::line(cflowmap, cv::Point(x, y),
               cv::Point(cvRound(x + fxy.x), cvRound(y + fxy.y)),
               cv::Scalar(0, 255, 0));
      cv::circle(cflowmap, cv::Point(x, y), 2, cv::Scalar(0, 255, 0), -1);
    }
  }
  return cflowmap;
}

cv::Mat drawNewSegmentedImage(cv::Mat optical_flow, cv::Mat old_frame) {
  cv::Mat new_frame = cv::Mat::zeros(old_frame.size(), CV_8UC1);

  for (int y = 0; y < old_frame.rows; y++) {
    for (int x = 0; x < old_frame.cols; x++) {
      if (old_frame.at<uchar>(y, x) > 0) {
        const cv::Point2d fxy = optical_flow.at<cv::Point2d>(y, x);
        int r = cvRound(y + fxy.y);
        int c = cvRound(x + fxy.x);
        if (r >= 0 && c >= 0 && r < new_frame.rows && c < new_frame.cols) {
          new_frame.at<uchar>(r, c) = 255;
        }
      }
    }
  }
  return new_frame;
}
