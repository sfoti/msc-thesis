// Copyright (c) 2017 Simone Foti <simofoti@gmail.com>

#ifndef SRC_TRACKING_TRACKING_H_
#define SRC_TRACKING_TRACKING_H_

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video/tracking.hpp"

cv::Mat segmentNewFrame(cv::Mat prev_frame, cv::Mat following_frame,
                        cv::Mat prev_segmentation);
void trackTwoImages(cv::Mat prev_frame, cv::Mat following_frame,
                    cv::Mat prev_segmentation);
cv::Mat drawOptFlowMap(cv::Mat in, cv::Mat prev, int step, double);
cv::Mat drawNewSegmentedImage(cv::Mat optical_flow, cv::Mat old_frame);

#endif  // SRC_TRACKING_TRACKING_H_
